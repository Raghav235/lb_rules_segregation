import pymongo
import json
from DbData import DbData


class DbOperations:

    def __init__(self, config):
        self.db_client = pymongo.MongoClient(config["mongo"]["connection_url"])
        self.db = self.db_client[config["mongo"]["db_name"]]
        self.db_collections = config["mongo"]["collections_name"]

    def push_db_data(self, db_data, command):
        index = list(self.db_collections).index(command)
        # self.db[self.db_collections[index]].drop()
        self.db[self.db_collections[index]].insert(db_data.get_json(), check_keys=False)

    def get_binding_cs_policies(self, command_name):
        collection = self.db["bind cs vserver"]
        return collection.find({"name": command_name})

    def get_vserver_binding_policies(self, command):
        collection = self.db["bind lb vserver"]
        return collection.find({"name": command})

    def get_service_group_binding_policies(self, command):
        collection = self.db["bind serviceGroup"]
        return collection.find({"name": command})

    def get_server_ip(self, command):
        collection = self.db["add server"]
        return collection.find_one({"name": command})

    def push_results(self,result):
        collection = self.db["results"]
        collection.insert(result, check_keys=False)

    def get_cs_vserver(self):
        collection = self.db["add cs vserver"]
        return collection.find()

    def get_responder_policies(self):
        collection = self.db["add responder policy"]
        return collection.find()

    def get_responder_action(self, name):
        collection = self.db["add responder action"]
        return collection.find_one({"name": name})

    def get_limit_identifier(self, name):
        collection = self.db["add ns limitIdentifier"]
        return collection.find_one({"name": name})

    def get_stream_selectors(self, name):
        collection = self.db["add stream selectors"]
        return collection.find_one({"name": name})

    def get_cs_policy(self, name):
        collection = self.db["add cs policy"]
        return collection.find_one({"name": name})

    def get_rewrite_policy(self, name):
        collection = self.db["add rewrite policy"]
        return collection.find_one({"name": name})

    def get_filter_policy(self, name):
        collection = self.db["add filter policy"]
        return collection.find_one({"name": name})