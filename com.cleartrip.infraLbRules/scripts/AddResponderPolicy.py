from DbData import DbData


class AddResponderPolicy:
    command_name = "add responder policy"

    def __init__(self):
        pass

    def parse_command(self, command):
        cmd_copy = command
        name = cmd_copy.replace(self.command_name + " ", "")
        name = name.split(' ')[0]
        args = cmd_copy.replace(self.command_name + " ", "").replace(name + " ", "")
        responder_action = str(args).rsplit(" ", 1)[1]
        tmp_str = str(args).rsplit(" ", 1)[0]
        limit_identifier = None
        if tmp_str.__contains__("SYS.CHECK_LIMIT"):
            idx = tmp_str.index("SYS.CHECK_LIMIT")
            idx1 = tmp_str.index("\"", idx)
            idx2 = tmp_str.index("\\\"", idx1)
            limit_identifier = tmp_str.__getslice__(idx1+1, idx2)
        args_map = {"responder_action": responder_action, "limitIdentifier": limit_identifier}
        db_data = DbData(self.command_name, name, args_map)
        return db_data
