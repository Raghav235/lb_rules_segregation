from LbFileOperationService import LbFileOperationService
from ResponderPolicyOperationService import ResponderPolicyOperationService
import shutil
from FileUtility import FileUtility

Config_File_Path = "/home/raghavgupta/PycharmProjects/infraLbRules/com.cleartrip.infraLbRules/config/config.yml"

if __name__ == "__main__":
    lbFileOperationService = LbFileOperationService(Config_File_Path)
    lbFileOperationService.readComments()
    lbFileOperationService.truncate_old_files()
    lbFileOperationService.segregate_master_file()
    lbFileOperationService.create_cs_db()
    lbFileOperationService.update_grouped_commands()
    fileUtility = FileUtility()
    fileUtility.copy_unprocessed_commands(Config_File_Path)
