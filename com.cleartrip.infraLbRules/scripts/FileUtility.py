import yaml
import shutil


class FileUtility:

    def __init__(self):
        self.first_occurance_lines = set()

    def config_file_reader(self, file_path):
        with open(file_path, 'r') as stream:
            try:
                return yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def read_file(self, file_path):
        with open(file_path, 'r') as rules:
            data = rules.read()
            return data

    def write_result_to_file(self, file_path, result):
        f = open(file_path, "w")
        f.write(result)
        f.close()

    def write_unprocessed_commands(self, location, lb_rules, commands_to_segregate):
        f = open(location, "a+")
        f.write("#Unprocessed Commands\n")
        for line in lb_rules.splitlines():
            flg = True
            for command in commands_to_segregate:
                if command in line:
                    flg = False
                    break
            if flg:
                f.write(line + "\n")
        f.close()

    def find_line_by_keyword(self, lb_rules, arr):
        for line in lb_rules.splitlines():
            if arr in line:
                return line

    def find_lines_by_keyword(self, lb_rules, arr):
        lines = list()
        for line in lb_rules.splitlines():
            if arr in line:
                lines.append(line)
        return lines

    def write_processed_line(self, location, line):
        if not line == None:
            if self.check_if_inserted(location, line):
                self.first_occurance_lines.add(line)
                line = "#" + line + "   #used at multiple places"
            f = open(location, "a+")
            f.write(line + "\n")
            f.close()

    def check_if_inserted(self, location, line):
        flg = False
        f = open(location, "a+")
        data = f.read()
        for x in data.splitlines():
            if line in x:
                flg = True
                break;
        f.close()
        return flg

    def is_selected_line(self, line):
        flg = False
        for command in self.first_occurance_lines:
            if command == line + "\n":
                flg = True
                break
        return flg

    def add_first_occurance_comments(self, location):
        if not self.first_occurance_lines is None:
            temp = open('temp', 'wb')
            with open(location, 'r') as f:
                for line in f:
                    if self.is_selected_line(line):
                        line = line.strip() + "     #used at multiple places\n"
                    temp.write(line)
            temp.close()
            shutil.move('temp', location)

    def copy_unprocessed_commands(self, config_path):
        config_yaml = self.config_file_reader(config_path)
        location = config_yaml["files"]["unprocessed_file"]["path"]
        rules = self.read_file(location)
        location2 = config_yaml["files"]["output_file"]["path"]
        f = open(location2, 'a')
        f.write(rules)
        f.close()

    def write_line(self, location, line):
        if not line is None:
            f = open(location, "a+")
            f.write(line + "\n")
            f.close()
