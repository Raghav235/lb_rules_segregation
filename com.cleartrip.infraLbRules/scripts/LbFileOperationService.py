from FileUtility import FileUtility
import Utils
from DbOperations import DbOperations
from Info import Info, InfoList, ResponderPolicyInfo, ResponderPolicyList
from Commands import Commands
import json


class LbFileOperationService:
    bind_cs_vserver = list()
    add_cs_policy = list()
    add_cs_vserver = list()
    add_lb_vserver = list()
    add_server = list()
    add_serviceGroup = list()
    bind_lb_vserver = list()
    bind_serviceGroup = list()
    add_responder_policy = list()
    add_responder_action = list()
    add_ns_limitIdentifier = list()
    add_stream_selector = list()
    add_filter_policy = list()
    add_filter_action = list()
    add_rewrite_policy = list()
    add_rewrite_action = list()
    cs_comments = dict()

    def __init__(self, config_yaml_path):
        self.fileUtility = FileUtility()
        self.config_yaml = self.fileUtility.config_file_reader(config_yaml_path)
        self.db_operation_service = DbOperations(self.config_yaml)

    def get_list(self, command):
        switcher = {
            "bind cs vserver": self.bind_cs_vserver,
            "add cs policy": self.add_cs_policy,
            "add cs vserver": self.add_cs_vserver,
            "add lb vserver": self.add_lb_vserver,
            "add server": self.add_server,
            "add serviceGroup": self.add_serviceGroup,
            "bind lb vserver": self.bind_lb_vserver,
            "bind serviceGroup": self.bind_serviceGroup,
            "add responder policy": self.add_responder_policy,
            "add responder action": self.add_responder_action,
            "add ns limitIdentifier": self.add_ns_limitIdentifier,
            "add stream selector": self.add_stream_selector,
            "add filter policy": self.add_filter_policy,
            "add filter action": self.add_filter_action,
            "add rewrite policy": self.add_rewrite_policy,
            "add rewrite action": self.add_rewrite_action
        }

        return switcher.get(command)

    def segregate_master_file(self):
        file_path = self.config_yaml["files"]["latest_file"]["path"]
        file_name = self.config_yaml["files"]["latest_file"]["name"]
        commands_to_segregate = self.config_yaml["commands"]
        lb_rules = self.fileUtility.read_file(file_path + file_name)
        location = self.config_yaml["files"]["unprocessed_file"]["path"]
        self.fileUtility.write_unprocessed_commands(location, lb_rules, commands_to_segregate)
        for command in commands_to_segregate:
            commands_list = Utils.segregate_command(lb_rules, command)
            db_data_list = Utils.parse_commands(commands_list, command)
            self.update_db(db_data_list, command)

    def update_db(self, db_data_list, command):
        command_list = self.get_list(command)
        for db_data in db_data_list:
            command_list.append(db_data)


    def create_cs_db(self):
        file_path = self.config_yaml["files"]["latest_file"]["path"]
        file_name = self.config_yaml["files"]["latest_file"]["name"]
        lb_rules = self.fileUtility.read_file(file_path + file_name)
        commands_list = self.add_cs_vserver
        info_list = list()
        for command in commands_list:
            info = self.segregate_commands(command.name)
            info_list.append(info)
        info_data_list = InfoList(info_list)
        location = self.config_yaml["files"]["output_file"]["path"]
        for unit_info in info_data_list.infos:
            self.write_grouped_cmnds_to_file(unit_info, lb_rules, location)
        # self.fileUtility.add_first_occurance_comments(location)
        print "Number of Groups: " + str(len(info_data_list.infos))

    def segregate_commands(self, command):
        try:
            binding_cs_vserver_policies = self.get_binding_vserver_policies(command)
            lb_servers = list()
            cs_policies = list()
            filter_policies = list()
            service_groups = list()
            servers = list()
            filter_actions = list()
            rewrite_policies = list()
            rewrite_actions = list()

            for bp in binding_cs_vserver_policies:
                x = bp.args
                if dict(x).has_key("lbvserver"):
                    if not lb_servers.__contains__(x["lbvserver"]):
                        lb_servers.append(x["lbvserver"])
                elif dict(x).has_key("targetLBVserver"):
                    if not lb_servers.__contains__(x["targetLBVserver"]):
                        lb_servers.append(x["targetLBVserver"])
                if dict(x).has_key("policyName"):
                    policy_name = x["policyName"]
                    if not self.get_cs_policy(policy_name) is None:
                        cs_policies.append(policy_name)
                    elif not self.get_filter_policy(policy_name) is None:
                        filter_policy = self.get_filter_policy(policy_name)
                        filter_policies.append(policy_name)
                        ignore_actions = ["DROP", "RESET"]
                        if filter_policy.args.has_key("reqAction"):
                            action = filter_policy.args["reqAction"]
                            if not action is None:
                                if not ignore_actions.__contains__(action):
                                    filter_actions.append(action)
                        if filter_policy.args.has_key("resAction"):
                            action = filter_policy.args["resAction"]
                            if not action is None:
                                if not ignore_actions.__contains__(action):
                                    filter_actions.append(action)
                    else:
                        rewrite_policy = self.get_rewrite_policy(policy_name)
                        if not rewrite_policy is None:
                            rewrite_policies.append(policy_name)
                            if rewrite_policy.args.has_key("action"):
                                rewrite_action = rewrite_policy.args["action"]
                                rewrite_actions.append(rewrite_action)

            for lb_vserver in lb_servers:
                vserver_binding_policies = self.get_vserver_binding_policies(lb_vserver)
                for binding_policies in vserver_binding_policies:
                    y = binding_policies.args
                    if dict(y).has_key("SERVICE_GROUP"):
                        service_groups.append(y["SERVICE_GROUP"])
                    if dict(y).has_key("-policyName"):
                        policy_name = y["-policyName"]
                        rewrite_policy = self.get_rewrite_policy(policy_name)
                        if not rewrite_policy is None:
                            if not rewrite_policies.__contains__(rewrite_policy):
                                rewrite_policies.append(policy_name)
                                if rewrite_policy.args.has_key("action"):
                                    rewrite_action = rewrite_policy.args["action"]
                                    rewrite_actions.append(rewrite_action)
            for ser_grp in service_groups:
                service_group_binding_policies = self.get_serviceGroup_bindingPolicies(ser_grp)
                for bp in service_group_binding_policies:
                    z = bp.args
                    if dict(z).has_key("SERVER_NAME"):
                        servers.append(z["SERVER_NAME"])
            info = Info(command, cs_policies, lb_servers, service_groups, servers, filter_policies, filter_actions,
                        rewrite_policies, rewrite_actions)
            return info
        except Exception as e:
            print "Exception occured for cs vserver: " + command
            print str(e)

    def write_grouped_cmnds_to_file(self, unit_info, lb_rules, location):
        old_comments = self.cs_comments[unit_info.name]
        self.fileUtility.write_line(location, "#@id:" + unit_info.name)
        self.fileUtility.write_line(location, "#@type:" + old_comments.type)
        self.fileUtility.write_line(location, "#@team:" + old_comments.team)
        self.fileUtility.write_line(location, "#@desc:" + old_comments.desc)
        self.fileUtility.write_line(location, "#@status:" + old_comments.status)

        cs_vserver = self.fileUtility.find_line_by_keyword(lb_rules, "add cs vserver " + unit_info.name)
        self.fileUtility.write_processed_line(location, cs_vserver)
        cs_binding_policies = self.fileUtility.find_lines_by_keyword(lb_rules, "bind cs vserver " + unit_info.name)
        for cs_binding_policy in cs_binding_policies:
            self.fileUtility.write_processed_line(location, cs_binding_policy)
        for cs_policy in unit_info.policies:
            cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add cs policy " + cs_policy)
            self.fileUtility.write_processed_line(location, cmd)
        for filter_policy in unit_info.filterPolicies:
            cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add filter policy " + filter_policy)
            self.fileUtility.write_processed_line(location, cmd)
        for filter_action in unit_info.filterAction:
            cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add filter action " + filter_action)
            self.fileUtility.write_processed_line(location, cmd)

        for rewrite_policy in unit_info.rewritePolicies:
            cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add rewrite policy " + rewrite_policy)
            self.fileUtility.write_processed_line(location, cmd)

        for rewrite_action in unit_info.rewriteActions:
            cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add rewrite action " + rewrite_action)
            self.fileUtility.write_processed_line(location, cmd)

        for lb_vserver in unit_info.lb_servers:
            cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add lb vserver " + lb_vserver)
            self.fileUtility.write_processed_line(location, cmd)
            binding_policies = self.fileUtility.find_lines_by_keyword(lb_rules, "bind lb vserver " + lb_vserver)
            for binding_policy in binding_policies:
                self.fileUtility.write_processed_line(location, binding_policy)
        for service_grp in unit_info.service_groups:
            cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add serviceGroup " + service_grp)
            self.fileUtility.write_processed_line(location, cmd)
            binding_policies = self.fileUtility.find_lines_by_keyword(lb_rules, "bind serviceGroup " + service_grp)
            for binding_policy in binding_policies:
                self.fileUtility.write_processed_line(location, binding_policy)
        for server in unit_info.servers:
            cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add server " + server)
            self.fileUtility.write_processed_line(location, cmd)
        self.fileUtility.write_processed_line(location, "\n")

    def get_binding_vserver_policies(self, command):
        binding_policies = list()
        for bp in self.bind_cs_vserver:
            if bp.name == command:
                binding_policies.append(bp)
        return binding_policies

    def get_cs_policy(self, policy_name):
        cs_policy = None
        for csp in self.add_cs_policy:
            if csp.name == policy_name:
                cs_policy = csp
                break
        return cs_policy

    def get_filter_policy(self, policy_name):
        filter_policy = None
        for fp in self.add_filter_policy:
            if fp.name == policy_name:
                filter_policy = fp
                break
        return filter_policy

    def get_rewrite_policy(self, policy_name):
        rewrite_policy = None
        for rp in self.add_rewrite_policy:
            if rp.name == policy_name:
                rewrite_policy = rp
                break
        return rewrite_policy

    def get_vserver_binding_policies(self, lb_vserver):
        binding_policies = list()
        for bp in self.bind_lb_vserver:
            if bp.name == lb_vserver:
                binding_policies.append(bp)
        return binding_policies

    def get_serviceGroup_bindingPolicies(self, ser_grp):
        binding_policies = list()
        for bp in self.bind_serviceGroup:
            if bp.name == ser_grp:
                binding_policies.append(bp)
        return binding_policies

    def update_grouped_commands(self):
        file_path = self.config_yaml["files"]["latest_file"]["path"]
        file_name = self.config_yaml["files"]["latest_file"]["name"]
        lb_rules = self.fileUtility.read_file(file_path + file_name)
        responder_policies = self.add_responder_policy
        responder_policy_list = list()
        for responder_policy in responder_policies:
            responder_policy_info = self.get_responder_policy_info(responder_policy)
            responder_policy_list.append(responder_policy_info)
        info_list = ResponderPolicyList(responder_policy_list)
        location = self.config_yaml["files"]["output_file"]["path"]
        for res_pol_inf in info_list.policy_list:
            self.write_reponder_commands(lb_rules, res_pol_inf, location)
        print "Number of Groups: " + str(len(info_list.policy_list))

    def get_responder_policy_info(self, responder_policy):
        policy_name = responder_policy.name
        action_name = responder_policy.args["responder_action"]
        limit_identifier = None
        stream_selector = None
        if responder_policy.args.has_key("limitIdentifier"):
            limit_identifier = responder_policy.args["limitIdentifier"]
            limit_idetifier_rule = self.get_limit_identifier(limit_identifier)
            if not limit_idetifier_rule is None:
                if limit_idetifier_rule.args.has_key("selectorName"):
                    stream_selector = limit_idetifier_rule.args["selectorName"]
        responder_policy_info = ResponderPolicyInfo(policy_name, action_name, limit_identifier, stream_selector)
        return responder_policy_info

    def write_reponder_commands(self, lb_rules, res_pol_inf, location):
        old_comments = self.cs_comments[res_pol_inf.name]
        self.fileUtility.write_line(location, "#@id:" + res_pol_inf.name)
        self.fileUtility.write_line(location, "#@type:" + old_comments.type)
        self.fileUtility.write_line(location, "#@team:" + old_comments.team)
        self.fileUtility.write_line(location, "#@desc:" + old_comments.desc)
        self.fileUtility.write_line(location, "#@status:" + old_comments.status)
        policy_cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add responder policy " + res_pol_inf.name)
        self.fileUtility.write_processed_line(location, policy_cmd)
        action_cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add responder action " + res_pol_inf.action)
        self.fileUtility.write_processed_line(location, action_cmd)
        if not res_pol_inf.limit_identifier is None:
            limit_identifier_cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add ns limitIdentifier " + res_pol_inf.limit_identifier)
            self.fileUtility.write_processed_line(location, limit_identifier_cmd)
        if not res_pol_inf.stream_selector is None:
            stream_selector_cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add stream selector " + res_pol_inf.stream_selector)
            self.fileUtility.write_processed_line(location, stream_selector_cmd)
        self.fileUtility.write_processed_line(location, "\n")

    def get_limit_identifier(self, limit_identifier):
        limitIdentifier = None
        for li in self.add_ns_limitIdentifier:
            if li.name == limit_identifier:
                limitIdentifier = li
                break
        return limitIdentifier

    def readComments(self):
        file_path = self.config_yaml["files"]["output_file"]["path"]
        lb_file_data = self.fileUtility.read_file(file_path)
        lb_rules = lb_file_data.splitlines()
        for line in lb_rules:
            if line.__contains__("#@id"):
                idx = lb_rules.index(line)
                name = line.split(":")[1]
                type = lb_rules[idx+1].split(":")[1]
                team = lb_rules[idx+2].split(":")[1]
                desc = lb_rules[idx+3].split(":")[1]
                status = lb_rules[idx+4].split(":")[1]
                command = Commands(type, team, desc, status)
                self.cs_comments.update({name: command})

    def truncate_old_files(self):
        file_path = self.config_yaml["files"]["output_file"]["path"]
        open(file_path, 'w').close()
        file_path = self.config_yaml["files"]["unprocessed_file"]["path"]
        open(file_path, 'w').close()