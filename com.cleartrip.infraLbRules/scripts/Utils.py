from ParserFactory import parser_config


def segregate_command(lb_rules, command_name):
    count = 0
    command_list = set()
    for command in lb_rules.splitlines():
        if command.__contains__(command_name):
            # print command
            command_list.add(command)
            count = count + 1
    print count
    return command_list


def parse_commands(commands_list, command):
    db_data_list = list()
    for cmd in commands_list:
        db_data = parser_config.get(command).parse_command(cmd)
        db_data_list.append(db_data)
    return db_data_list
