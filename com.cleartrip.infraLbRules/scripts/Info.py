import json


class Info:
    def __init__(self, name, policies, lb_servers, service_groups, servers, filter_policies, filter_action, rewrite_policies, rewrite_actions):
        self.name = name
        self.policies = policies
        self.lb_servers = lb_servers
        self.service_groups = service_groups
        self.servers = servers
        self.filterPolicies = filter_policies
        self.filterAction = filter_action
        self.rewritePolicies = rewrite_policies
        self.rewriteActions = rewrite_actions

    def get_mongo_data(self):
        data = dict()
        data.update({"Policies": self.policies})
        data.update({"Lb_Servers": self.lb_servers})
        data.update({"Service_group": self.service_groups})
        data.update({"servers": self.servers})
        data.update({self.name: self.name})
        return data


class InfoList:
    def __init__(self, infos):
        self.infos = infos

    def get_json(self):
        final_json = dict()
        for info in self.infos:
            info_json = dict()
            info_json.update({"Policies": info.policies})
            info_json.update({"Lb_Servers": info.lb_servers})
            info_json.update({"Service_group": info.service_groups})
            info_json.update({"servers": info.servers})
            final_json.update({info.name: info_json})
        return final_json

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


class ResponderPolicyInfo:

    def __init__(self, name, action, limit_identifier, stream_selector):
        self.name = name
        self.action = action
        self.limit_identifier = limit_identifier
        self.stream_selector = stream_selector


class ResponderPolicyList:

    def __init__(self, policy_list):
        self.policy_list = policy_list
