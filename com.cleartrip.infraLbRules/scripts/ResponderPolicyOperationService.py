from FileUtility import FileUtility
from DbOperations import DbOperations
from Info import ResponderPolicyInfo
from Info import ResponderPolicyList
import Utils


class ResponderPolicyOperationService:

    def __init__(self, config_yaml_path):
        self.fileUtility = FileUtility()
        self.config_yaml = self.fileUtility.config_file_reader(config_yaml_path)
        self.db_operation_service = DbOperations(self.config_yaml)

    def segregate_master_file(self):
        file_path = self.config_yaml["files"]["latest_file"]["path"]
        file_name = self.config_yaml["files"]["latest_file"]["name"]
        commands_to_segregate = self.config_yaml["responder_commands"]
        lb_rules = self.fileUtility.read_file(file_path + file_name)
        location = self.config_yaml["files"]["unprocessed_file"]["path"]
        # self.fileUtility.write_unprocessed_commands(location, lb_rules, commands_to_segregate)
        for command in commands_to_segregate:
            commands_list = Utils.segregate_command(lb_rules, command)
            db_data_list = Utils.parse_commands(commands_list, command)
            self.update_db(db_data_list, command)

    def update_db(self, db_data_list, command):
        for db_data in db_data_list:
            self.db_operation_service.push_db_data(db_data, command)

    def update_grouped_commands(self):
        file_path = self.config_yaml["files"]["latest_file"]["path"]
        file_name = self.config_yaml["files"]["latest_file"]["name"]
        lb_rules = self.fileUtility.read_file(file_path + file_name)
        responder_policies = self.db_operation_service.get_responder_policies()
        responder_policy_list = list()
        for responder_policy in responder_policies:
            responder_policy_info = self.get_responder_policy_info(responder_policy)
            responder_policy_list.append(responder_policy_info)
        info_list = ResponderPolicyList(responder_policy_list)
        location = self.config_yaml["files"]["output_file"]["path"]
        for res_pol_inf in info_list.policy_list:
            self.write_grouped_cmnds_to_file(lb_rules, res_pol_inf, location)
        print "Number of Groups: " + str(len(info_list.policy_list))

    def get_responder_policy_info(self, responder_policy):
        policy_name = responder_policy["name"]
        action_name = responder_policy["responder_action"]
        limit_identifier = None
        stream_selector = None
        if not responder_policy["limitIdentifier"] is None:
            limit_identifier = responder_policy["limitIdentifier"]
            limit_idetifier_rule = self.db_operation_service.get_limit_identifier(limit_identifier)
            if not limit_idetifier_rule is None:
                stream_selector = limit_idetifier_rule["selectorName"]
        responder_policy_info = ResponderPolicyInfo(policy_name, action_name, limit_identifier, stream_selector)
        return responder_policy_info

    def write_grouped_cmnds_to_file(self, lb_rules, res_pol_inf, location):
        self.fileUtility.write_processed_line(location, "#" + res_pol_inf.name)
        policy_cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add responder policy " + res_pol_inf.name)
        self.fileUtility.write_processed_line(location, policy_cmd)
        action_cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add responder action " + res_pol_inf.action)
        self.fileUtility.write_processed_line(location, action_cmd)
        if not res_pol_inf.limit_identifier is None:
            limit_identifier_cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add ns limitIdentifier " + res_pol_inf.limit_identifier)
            self.fileUtility.write_processed_line(location, limit_identifier_cmd)
        if not res_pol_inf.stream_selector is None:
            stream_selector_cmd = self.fileUtility.find_line_by_keyword(lb_rules, "add stream selector " + res_pol_inf.stream_selector)
            self.fileUtility.write_processed_line(location, stream_selector_cmd)
        self.fileUtility.write_processed_line(location, "\n")