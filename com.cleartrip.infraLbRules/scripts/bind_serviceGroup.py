from DbData import DbData


class bind_serviceGroup:
    command_name = "bind serviceGroup"

    def __init__(self):
        pass

    def parse_command(self, command):
        cmd_copy = command
        name = cmd_copy.replace(self.command_name + " ", "")
        name = name.split(' ')[0]
        args = cmd_copy.replace(self.command_name + " ", "").replace(name + " ", "")
        args_map = dict()
        if args[0] != '-':
            tmp_args = args.split(" ", 2)
            if len(tmp_args) > 2:
                args = tmp_args[2]
            else:
                args = ""
            server_dict = {"SERVER_NAME": tmp_args[0]}
            args_map.update(server_dict)
            server_dict = {"PORT": tmp_args[1]}
            args_map.update(server_dict)
        args = args.split(' -')
        for ar in args:
            if ar != "":
                key_value = ar.split(" ", 1)
                key = key_value[0]
                value = key_value[1]
                tmp_dict = {key: value}
                args_map.update(tmp_dict)
        db_data = DbData(self.command_name, name, args_map)
        return db_data
