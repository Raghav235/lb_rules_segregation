from bind_cs_vserver import bind_cs_vserver
from Policies import add_cs_policy,AddFilterPolicy, AddRewritePolicy
from add_cs_vserver import add_cs_vserver
from add_lb_vserver import add_lb_vserver
from add_server import add_server
from add_servicegroup import add_servicegroup
from bind_lb_vserver import bind_lb_vserver
from bind_serviceGroup import bind_serviceGroup
from AddResponderPolicy import AddResponderPolicy
from Actions import AddResponderAction, AddFilterAction, AddRewriteAction
from AddLimitIdentifier import AddLimitIdentifier
from AddStreamSelector import AddStreamSelector

parser_config = {
    "bind cs vserver": bind_cs_vserver(),
    "add cs policy": add_cs_policy(),
    "add cs vserver": add_cs_vserver(),
    "add lb vserver": add_lb_vserver(),
    "add server": add_server(),
    "add serviceGroup": add_servicegroup(),
    "bind lb vserver": bind_lb_vserver(),
    "bind serviceGroup": bind_serviceGroup(),
    "add responder policy": AddResponderPolicy(),
    "add responder action": AddResponderAction(),
    "add ns limitIdentifier": AddLimitIdentifier(),
    "add stream selector": AddStreamSelector(),
    "add filter policy": AddFilterPolicy(),
    "add filter action": AddFilterAction(),
    "add rewrite policy": AddRewritePolicy(),
    "add rewrite action": AddRewriteAction()
}
