from DbData import DbData


class AddStreamSelector:
    command_name = "add stream selector"

    def __init__(self):
        pass

    def parse_command(self, command):
        cmd_copy = command
        name = cmd_copy.replace(self.command_name + " ", "")
        name = name.split(' ')[0]
        args = cmd_copy.replace(self.command_name + " ", "").replace(name + " ", "")
        args_map = {"ARGS": args}
        db_data = DbData(self.command_name, name, args_map)
        return db_data