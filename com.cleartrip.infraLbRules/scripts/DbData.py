import json


class DbData:

    def __init__(self, command, name, args):
        self.command = command
        self.name = name
        self.args = args

    def get_args(self):
        return self.args

    def get_name(self):
        return self.name

    def get_command(self):
        return self.command

    def get_json(self):
        result_map = dict(command=self.command, name=self.name)
        for key, value in self.args.items():
            tmp_dict = {key: value}
            result_map.update(tmp_dict)
        return result_map
