from DbData import DbData


class add_cs_policy:
    command_name = "add cs policy"

    def __init__(self):
        pass

    def parse_command(self, command):
        cmd_copy = command
        name = cmd_copy.replace(self.command_name + " ", "")
        name = name.split(' ')[0]
        args = cmd_copy.replace(self.command_name + " ", "").replace(name + " -", "")
        args_map = dict()
        args = args.split(' -')
        for ar in args:
            key_value = ar.split(" ", 1)
            key = key_value[0]
            value = key_value[1]
            tmp_dict = {key: value}
            args_map.update(tmp_dict)
        db_data = DbData(self.command_name, name, args_map)
        return db_data


class AddFilterPolicy:
    command_name = "add filter policy"

    def __init__(self):
        pass

    def parse_command(self, command):
        cmd_copy = command
        name = cmd_copy.replace(self.command_name + " ", "")
        name = name.split(' ')[0]
        args = cmd_copy.replace(self.command_name + " ", "").replace(name + " -", "")
        args_map = dict()
        args = args.split(' -')
        for ar in args:
            key_value = ar.split(" ", 1)
            key = key_value[0]
            value = key_value[1]
            tmp_dict = {key: value}
            args_map.update(tmp_dict)
        db_data = DbData(self.command_name, name, args_map)
        return db_data


class AddRewritePolicy:

    command_name = "add rewrite policy"

    def __init__(self):
        pass

    def parse_command(self, command):
        cmd_copy = command
        name = cmd_copy.replace(self.command_name + " ", "")
        name = name.split(' ')[0]
        args = cmd_copy.replace(self.command_name + " ", "").replace(name + " ", "")
        args_map = dict()
        action_name = args.rsplit(" ", 1)[1]
        args_map.update({"action": action_name})
        args = args.rsplit(" ", 1)[0]
        args_map.update({"args": args})
        db_data = DbData(self.command_name, name, args_map)
        return db_data