from DbData import DbData


class add_cs_vserver:
    command_name = "add cs vserver"

    def __init__(self):
        pass

    def parse_command(self, command):
        cmd_copy = command
        name = cmd_copy.replace(self.command_name + " ", "")
        name = name.split(' ')[0]
        args = cmd_copy.replace(self.command_name + " ", "").replace(name + " ", "")
        args_map = dict()
        args = args.split(' -')
        for ar in args:
            key_value = ar.split(" ", 1)
            key = key_value[0]
            value = key_value[1]
            tmp_dict = {key: value}
            args_map.update(tmp_dict)
        db_data = DbData(self.command_name, name, args_map)
        return db_data
